from django.shortcuts import render
from django.core.context_processors import csrf

# Create your views here.
class AboutView():
    def view(request):
        args = {}
        args.update(csrf(request))

        print(request)
        #if request.alert:
        #    args['alert'] = response.alert
        return render(request, "about.html", args)
